<div align="center">
<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="Ansible logo"></a>
 </a>
</p>

  [![Status](https://img.shields.io/badge/status-STATUS-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-2.10-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/PROJECTID)]() 
  [![License](https://img.shields.io/badge/license-LICENSE-blue.svg)](/LICENSE)

</div>

---

# System Common

Ansible role for common (base) configuration for machines managed by Ansible.

- Updates hostname and hosts file. Tag `common_host`
- Normalizes locales and local time. Tag `common_locales`
- Installs common packages. Tag `common_packages`

## 🦺 Requirements

*None*

## 🗃️ Role Variables

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

**Example:**

Available variables are listed below, along with default values (see `defaults/main.yml`):

```yaml
    # Choose the version to be installed
    package_version: '0.1'
    # Default path
    package_path: '/opt/package'
```

## 📦 Dependencies

*None*

## 🤹 Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: servers
      roles:
         - sys-common
```

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
